from array import *


class ChallengeDay4o1:
    def __init__(self):
        pass

    def challenge_4_1(self):
        throws = ["84", "28", "29", "75", "58", "71", "26", "6", "73", "74", "41", "39", "87", "37", "16", "79", "55", "60", "62", "80", "64", "95", "46", "15", "5", "47", "2", "35", "32", "78", "89", "90", "96", "33", "4", "69", "42", "30", "54", "85", "65", "83", "44", "63", "20", "17", "66", "81", "67",
                  "77", "36", "68", "82", "93", "10", "25", "9", "34", "24", "72", "91", "88", "11", "38", "3", "45", "14", "56", "22", "61", "97", "27", "12", "48", "18", "1", "31", "98", "86", "19", "99", "92", "8", "43", "52", "23", "21", "0", "7", "50", "57", "70", "49", "13", "51", "40", "76", "94", "53", "59"]

        testThrows = ["7", "4", "9", "5", "11", "17", "23", "2", "0", "14", "21", "24", "10",
                      "16", "13", "6", "15", "25", "12", "22", "18", "20", "8", "19", "3", "26", "1"]

        bingocards = []

        with open('401input.txt') as f:
            bingoRow = []
            contents = f.readlines()
            for textLines, line in enumerate(contents):
                if(line != "\n"):
                    bingoRow.append(line.strip())

                if(len(bingoRow) == 5):
                    line.replace('\n', '')
                    bingocards.append(BingoCard(bingoRow))
                    bingoRow = []

        for throw in throws:
            for bingoCard in bingocards:
                bingo = bingoCard.checkForNumber(throw)
                if(bingo == True):
                    bingoCard.printCard(int(throw))
                    exit()


class BingoCard:
    theCard = []

    def __init__(self, bingoRowsForThisCard):
        self.theCard = [[0 for x in range(5)] for y in range(5)]
        for rowNumber, bingoRow in enumerate(bingoRowsForThisCard):
            bingoColumn = bingoRow.split()
            for column, bingoNumber in enumerate(bingoColumn):
                self.theCard[rowNumber][column] = (bingoNumber)

    def checkForNumber(self, number):
        for rowindx, row in enumerate(self.theCard):
            for columnindx, column in enumerate(row):
                if(column == number):
                    self.theCard[rowindx][columnindx] = "X"
                    # check if we have bingo
                    for rowy in self.theCard:
                        if(rowy.count("X") == 5):
                            print("BINGO")
                            return True

                        vertialBingo = 0
                        for column in rowy:
                            if(column == "X"):
                                vertialBingo += 1

                        if(vertialBingo == 5):
                            print("BINGO")
                            return True

        return False

    def printCard(self, bingoNumber):
        total = 0
        for row in self.theCard:
            for column in row:
                if (column != "X"):
                    total += int(column)

        print(total * bingoNumber)


staticmethod(ChallengeDay4o1.challenge_4_1)
