from ChallengeDay1o1 import ChallengeDay1o1
from ChallengeDay1o2 import ChallengeDay1o2
from ChallengeDay2o1 import ChallengeDay2o1
from ChallengeDay2o2 import ChallengeDay2o2
from ChallengeDay3o1 import ChallengeDay3o1
from ChallengeDay3o2 import ChallengeDay3o2
from ChallengeDay4o1 import ChallengeDay4o1
from ChallengeDay4o2 import ChallengeDay4o2


def main():
    print('------------ DAY 1 ------------')
    ChallengeDay1o1().challenge_1_1()
    ChallengeDay1o2().challenge_1_2()
    print('------------ DAY 2 ------------')
    ChallengeDay2o1().challenge_2_1()
    ChallengeDay2o2().challenge_2_2()
    print('------------ DAY 3 ------------')
    ChallengeDay3o1().challenge_3_1()
    ChallengeDay3o2().challenge_3_2()
    print('------------ DAY 4 ------------')
    ChallengeDay4o1().challenge_4_1()
    ChallengeDay4o2().challenge_4_2()


main()
